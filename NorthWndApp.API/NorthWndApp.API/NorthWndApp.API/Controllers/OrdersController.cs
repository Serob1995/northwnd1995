﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Entities;

namespace NorthWndApp.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderOperations _orderOperations;

        public OrdersController(IOrderOperations orderOperations)
        {
            _orderOperations = orderOperations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _orderOperations.GetOrder();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetOrderId([FromRoute] int id)
        {
            var result = _orderOperations.GetOrderId(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [HttpPost]

        public IActionResult AddOrders([FromBody] AddOrderModel model) 
        {
            var result = _orderOperations.AddOrder(model);
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            return Created("", result);
        }

        [HttpPut]

        public IActionResult UpdateOrder([FromBody] UpdateOrderModel model)
        {
            var result = _orderOperations.UpdateOrder(model);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteOrder([FromBody] int id) 
        {
            var result = _orderOperations.DeleteOrder(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
