﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Business_Models.Suppliers_Models;

namespace NorthWndApp.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SuppliersController : ControllerBase
    {
        private readonly ISupplierOperations _supplierOperations;

        public SuppliersController(ISupplierOperations supplierOperations)
        {
            _supplierOperations = supplierOperations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _supplierOperations.GetSupplier();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetOrderId([FromRoute] int id)
        {
            var result = _supplierOperations.GetSupplierId(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [HttpPost]

        public IActionResult AddSupplier([FromBody] AddSupplierModel model)
        {
            var result = _supplierOperations.AddSupplier(model);
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            return Created("", result);
        }

        [HttpPut]

        public IActionResult UpdateSupplier([FromBody] UpdateSupplierModel model)
        {
            var result = _supplierOperations.UpdateSupplier(model);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteSupplier([FromBody] int id)
        {
            var result = _supplierOperations.DeleteSupplier(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
