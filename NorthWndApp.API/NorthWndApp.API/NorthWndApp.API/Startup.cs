using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NorthWndApp.BLL.Operations;
using NorthWndApp.Core.Abstractions;
using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Entities;
using NorthWndAPP.DAL;
using NorthWndAPP.DAL.Repositories;

namespace NorthWndApp.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddDbContext<NORTHWNDContext>(x =>
            {
                x.UseSqlServer(Configuration.GetConnectionString("default"));
            });
            //services.AddScoped<IProblemsTaskOperations, ProblemsTaskOperations > ();
            services.AddScoped<IOrderOperations, OrderOperations>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<ISupplierOperations, SupplierOperations>();
            services.AddScoped<ISupplierRepository, SupplierRepository>();
            services.AddScoped<IRepositoryManager, RepositoryManager>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
