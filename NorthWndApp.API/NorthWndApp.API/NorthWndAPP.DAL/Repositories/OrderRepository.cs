﻿using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndAPP.DAL.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(NORTHWNDContext dbContext)
            : base(dbContext)
        {
        }
    }
}
