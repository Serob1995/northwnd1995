﻿using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndAPP.DAL.Repositories
{
    public class SupplierRepository: RepositoryBase<Supplier>, ISupplierRepository
    {
        public SupplierRepository(NORTHWNDContext dbContext)
            : base(dbContext) 
        {
        } 
    }
}
