﻿using NorthWndApp.Core.Abstractions;
using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Entities;
using NorthWndAPP.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace NorthWndAPP.DAL
{
    public class RepositoryManager: IRepositoryManager
    {
        private readonly NORTHWNDContext _dbContext;
        public RepositoryManager(NORTHWNDContext dbDontext) 
        {
            _dbContext = dbDontext;
        }

        private IOrderRepository _orders;

        public IOrderRepository Orders => _orders ?? (_orders = new OrderRepository(_dbContext));
        private ISupplierRepository _suppliers;
        public ISupplierRepository Suppliers => _suppliers ?? (_suppliers = new SupplierRepository(_dbContext));
        public IDatabaseTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return new DatabaseTransaction(_dbContext, isolationLevel);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }



    }
}
