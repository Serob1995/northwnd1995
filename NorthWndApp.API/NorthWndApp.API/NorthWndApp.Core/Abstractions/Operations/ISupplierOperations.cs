﻿using NorthWndApp.Core.Business_Models.Suppliers_Models;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Operations
{
    public interface ISupplierOperations
    {
        IEnumerable<SupplierViewModel> GetSupplier();
        Supplier GetSupplierId(int id);
        Supplier AddSupplier(AddSupplierModel model);
        Supplier UpdateSupplier(UpdateSupplierModel model);
        Supplier DeleteSupplier(int id);
    }
}
