﻿using NorthWndApp.Core.Business_Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Operations
{
    public interface IProblemsTaskOperations
    {
        IEnumerable<InventoryListModel20> GetInventoryList();
    }
}
