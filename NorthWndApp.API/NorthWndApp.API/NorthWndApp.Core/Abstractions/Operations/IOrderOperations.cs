﻿using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Operations
{
    public interface IOrderOperations
    {
        IEnumerable<OrderViewModel> GetOrder();
        Order GetOrderId(int id);
        Order AddOrder(AddOrderModel model);
        Order UpdateOrder(UpdateOrderModel model);
        Order DeleteOrder(int id);
    }
}
