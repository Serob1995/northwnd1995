﻿using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Repositories
{
    public interface IProblemTasksRepository : IRepositoryBase<Category>
    {
        IEnumerable<InventoryListModel20> GetInventoryList();
    }
}
