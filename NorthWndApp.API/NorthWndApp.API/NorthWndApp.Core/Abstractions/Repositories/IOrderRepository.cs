﻿using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Repositories
{
    public interface IOrderRepository: IRepositoryBase<Order>
    {

    }
}
