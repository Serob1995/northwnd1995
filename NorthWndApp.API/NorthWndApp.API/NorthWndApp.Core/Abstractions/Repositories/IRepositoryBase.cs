﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void Update(T entity);
        IEnumerable<T> GetAll();
        T Get(int id);
        bool Any(Func<T, bool> condition);
    }
}
