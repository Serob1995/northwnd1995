﻿using NorthWndApp.Core.Abstractions;
using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Business_Models.Suppliers_Models;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWndApp.BLL.Operations
{
    public class SupplierOperations : ISupplierOperations
    {
        private readonly IRepositoryManager _repositories;

        public SupplierOperations(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }
        public IEnumerable<SupplierViewModel> GetSupplier()
        {
            var data = _repositories.Suppliers.GetAll();

            var result = data.Select(x => new SupplierViewModel
            {
                SupplierId = x.SupplierId,
                CompanyName = x.CompanyName,
                ContactName = x.ContactName,
                ContactTitle = x.ContactTitle,
                Address = x.Address,
                City = x.City,
                Region = x.Region,
                PostalCode = x.PostalCode,
                Country = x.Country,
                Phone = x.Phone,
                Fax = x.Fax,
                HomePage = x.HomePage
            });
            return result;
        }

        public Supplier GetSupplierId(int id)
        {
            Supplier supplier;
            supplier = _repositories.Suppliers.Get(id);
            return supplier;
        }

        public Supplier AddSupplier(AddSupplierModel model)
        {
            Supplier supplier = new Supplier();
            supplier.CompanyName = model.CompanyName;
            supplier.ContactName = model.ContactName;
            supplier.ContactTitle = model.ContactTitle;
            supplier.Address = model.Address;
            supplier.City = model.City;
            supplier.Region = model.Region;
            supplier.PostalCode = model.PostalCode;
            supplier.Country = model.Country;
            supplier.Phone = model.Phone;
            supplier.Fax = model.Fax;
            supplier.HomePage = model.HomePage;
            _repositories.Suppliers.Add(supplier);
            _repositories.SaveChanges();
            return supplier;
        }

        public Supplier UpdateSupplier(UpdateSupplierModel model)
        {
            Supplier currentSupplier = new Supplier();
            currentSupplier.SupplierId = model.SupplierId;
            if (model.CompanyName != null)
            {
                currentSupplier.CompanyName = model.CompanyName;
            }
            if (model.ContactName != null)
            {
                currentSupplier.ContactName = model.ContactName;
            }
            if (model.ContactTitle != null)
            {
                currentSupplier.ContactTitle = model.ContactTitle;
            }
            if (model.Address != null)
            {
                currentSupplier.Address = model.Address;
            }
            if (model.Region != null)
            {
                currentSupplier.Region = model.Region;
            }
            if (model.City != null)
            {
                currentSupplier.City = model.City;
            }
            if (model.Country != null)
            {
                currentSupplier.Country = model.Country;
            }
            if (model.PostalCode != null)
            {
                currentSupplier.PostalCode = model.PostalCode;
            }
            if (model.Phone != null)
            {
                currentSupplier.Phone = model.Phone;
            }
            if (model.Fax != null)
            {
                currentSupplier.Fax = model.Fax;
            }
            if (model.HomePage != null)
            {
                currentSupplier.HomePage = model.HomePage;
            }
            _repositories.Suppliers.Update(currentSupplier);
            _repositories.SaveChanges();
            return currentSupplier;
        }

        public Supplier DeleteSupplier(int id)
        {
            Supplier supplier;
            supplier = _repositories.Suppliers.Get(id);
            _repositories.Suppliers.Remove(supplier);
            _repositories.SaveChanges();
            return supplier;
        }
    }
}
