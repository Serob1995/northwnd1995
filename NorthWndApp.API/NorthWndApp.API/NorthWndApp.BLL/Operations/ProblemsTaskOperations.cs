﻿using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Business_Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.BLL.Operations
{
    public class ProblemsTaskOperations : IProblemsTaskOperations
    {
        private readonly IProblemTasksRepository _orderRepository;

        public ProblemsTaskOperations(IProblemTasksRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public IEnumerable<InventoryListModel20> GetInventoryList()
        {
            return _orderRepository.GetInventoryList();
        }

    }
}
