﻿using NorthWndApp.Core.Abstractions;
using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Abstractions.Repositories;
using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWndApp.BLL.Operations
{
    public class OrderOperations : IOrderOperations
    {
        private readonly IRepositoryManager _repositories;

        public OrderOperations(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }

        public IEnumerable<OrderViewModel> GetOrder()
        {
            var data = _repositories.Orders.GetAll();

            var result = data.Select(x => new OrderViewModel
            {
                OrderId = x.OrderId,
                OrderDate = x.OrderDate,
                RequiredDate = x.RequiredDate,
                ShippedDate = x.ShippedDate,
                ShipVia = x.ShipVia,
                Freight = x.Freight,
                ShipRegion = x.ShipRegion,
                ShipPostalCode = x.ShipPostalCode,
                ShipCountry = x.ShipCountry,
                ShipCity = x.ShipCity,
                ShipAddress = x.ShipAddress,
                ShipName = x.ShipName
            });
            return result;
        }

        public Order GetOrderId(int id)
        {
            Order order;
            order = _repositories.Orders.Get(id);
            return order;
        }

        public Order AddOrder(AddOrderModel model)
        {
            using (var transaction = _repositories.BeginTransaction())
            {
                try
                {
                    Order order = new Order();
                    order.OrderDate = model.OrderDate;
                    order.RequiredDate = model.RequiredDate;
                    order.ShippedDate = model.ShippedDate;
                    order.ShipVia = model.ShipVia;
                    order.Freight = model.Freight;
                    order.ShipRegion = model.ShipRegion;
                    order.ShipPostalCode = model.ShipPostalCode;
                    order.ShipCountry = model.ShipCountry;
                    order.ShipCity = model.ShipCity;
                    order.ShipAddress = model.ShipAddress;
                    order.ShipName = model.ShipName;
                    _repositories.Orders.Add(order);
                    _repositories.SaveChanges();
                    transaction.Commit();
                    return order;
                }
                catch (System.Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public Order UpdateOrder(UpdateOrderModel model)
        {
            Order currentOrder = new Order();
            currentOrder.OrderId = model.OrderId;
            if (model.OrderDate != null)
            {
                currentOrder.OrderDate = model.OrderDate;
            }
            if (model.RequiredDate != null)
            {
                currentOrder.RequiredDate = model.RequiredDate;
            }
            if (model.ShippedDate != null)
            {
                currentOrder.ShippedDate = model.ShippedDate;
            }
            if (model.ShipVia.HasValue)
            {
                currentOrder.ShipVia = model.ShipVia;
            }
            if (model.ShipRegion != null)
            {
                currentOrder.ShipRegion = model.ShipRegion;
            }
            if (model.ShipCountry != null)
            {
                currentOrder.ShipCountry = model.ShipCountry;
            }
            if (model.ShipCity != null)
            {
                currentOrder.ShipCity = model.ShipCity;
            }
            if (model.ShipPostalCode != null)
            {
                currentOrder.ShipPostalCode = model.ShipPostalCode;
            }
            if (model.ShipName != null)
            {
                currentOrder.ShipName = model.ShipName;
            }
            _repositories.Orders.Update(currentOrder);
            _repositories.SaveChanges();
            return currentOrder;
        }

        public Order DeleteOrder(int id)
        {
            Order order;
            order = _repositories.Orders.Get(id);
            _repositories.Orders.Remove(order);
            _repositories.SaveChanges();
            return order;
        }
    }
}
