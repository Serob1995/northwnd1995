﻿using NorthWndApp.Core.Abstractions.Operations;
using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWndApp.BLL.Operations
{
    public class OrderOperations : IOrderOperations
    {
        private readonly NORTHWNDContext _dbContext;

        public OrderOperations(NORTHWNDContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<OrderViewModel> GetOrders()
        {
            var data = _dbContext.Orders.ToArray();

            var result = data.Select(x => new OrderViewModel
            {
                OrderId = x.OrderId,
                OrderDate = x.OrderDate,
                RequiredDate = x.RequiredDate,
                ShippedDate = x.ShippedDate,
                ShipVia = x.ShipVia,
                Freight = x.Freight,
                ShipRegion = x.ShipRegion,
                ShipPostalCode = x.ShipPostalCode,
                ShipCountry = x.ShipCountry,
                ShipCity = x.ShipCity,
                ShipAddress = x.ShipAddress,
                ShipName = x.ShipName
            });
            return result;
        }

        public Order GetOrdersId(int id)
        {
            Order order;
            order = _dbContext.Orders.Find(id);
            return order;
        }

        public Order AddOrders(AddOrderModel model)
        {
            Order order = new Order();
            order.OrderDate = model.OrderDate;
            order.RequiredDate = model.RequiredDate;
            order.ShippedDate = model.ShippedDate;
            order.ShipVia = model.ShipVia;
            order.ShipRegion = model.ShipRegion;
            order.ShipPostalCode = model.ShipPostalCode;
            order.ShipCountry = model.ShipCountry;
            order.ShipCity = model.ShipCity;
            order.ShipAddress = model.ShipAddress;
            order.ShipName = model.ShipName;
            _dbContext.Orders.Add(order);
            _dbContext.SaveChanges();
            return order;
        }

        public Order UpdateOrder(UpdateOrderModel model)
        {
            Order currentOrder = new Order();
            currentOrder.OrderId = model.OrderId;
            if (model.OrderDate != null)
            {
                currentOrder.OrderDate = model.OrderDate;
            }
            if (model.RequiredDate != null)
            {
                currentOrder.RequiredDate = model.RequiredDate;
            }
            if (model.ShippedDate != null)
            {
                currentOrder.ShippedDate = model.ShippedDate;
            }
            if (model.ShipVia != null)
            {
                currentOrder.ShipVia = model.ShipVia;
            }
            if (model.ShipRegion != null)
            {
                currentOrder.ShipRegion = model.ShipRegion;
            }
            if (model.ShipCountry != null)
            {
                currentOrder.ShipCountry = model.ShipCountry;
            }
            if (model.ShipCity != null)
            {
                currentOrder.ShipCity = model.ShipCity;
            }
            if (model.ShipPostalCode != null)
            {
                currentOrder.ShipPostalCode = model.ShipPostalCode;
            }
            if (model.ShipName != null)
            {
                currentOrder.ShipName = model.ShipName;
            }
            _dbContext.Orders.Update(currentOrder);
            _dbContext.SaveChanges();
            return currentOrder;
        }

        public Order DeleteOrder(int id)
        {
            Order order;
            order = _dbContext.Orders.Find(id);
            _dbContext.Orders.Remove(order);
            _dbContext.SaveChanges();
            return order;
        }

        public IEnumerable<Task29Model> GetDataFromTask29()
        {
            var data = _dbContext.Orders.ToList();
            var data1 = _dbContext.OrderDetails.ToList();
            var data2 = _dbContext.Employees.ToList();
            var data3 = _dbContext.Products.ToList();
            var result = new Task29Model();
            result = (Task29Model)(from e in _dbContext.Employees
                          join o in _dbContext.Orders on e.EmployeeId equals o.EmployeeId
                          join od in _dbContext.OrderDetails on o.OrderId equals od.OrderId
                          join p in _dbContext.Products on od.ProductId equals p.ProductId
                          select new { e.EmployeeId, e.LastName, o.OrderId, p.ProductName, od.Quantity });



            return (IEnumerable<Task29Model>)result;
        }
    }
}
