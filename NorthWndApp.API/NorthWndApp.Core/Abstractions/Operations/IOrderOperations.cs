﻿using NorthWndApp.Core.Business_Models;
using NorthWndApp.Core.Business_Models.OrdersModels;
using NorthWndApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWndApp.Core.Abstractions.Operations
{
    public interface IOrderOperations
    {
        IEnumerable<OrderViewModel> GetOrders();
        Order GetOrdersId(int id);
        Order AddOrders(AddOrderModel model);
        Order UpdateOrder(UpdateOrderModel model);
        Order DeleteOrder(int id);

        IEnumerable<Task29Model> GetDataFromTask29();
    }
}
