﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWndApp.Core.Business_Models
{
    public class Task29Model
    {
        // e.EmployeeId, e.LastName, o.OrderId, p.ProductName, od.Quantity 
        public int EmployeeId { get; set; }
        public string LastName { get; set; }
        public int OrderId { get; set; }
        public string ProductName { get; set; }
        public short Quantity { get; set; }

    }
}
